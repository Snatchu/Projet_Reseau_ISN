<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<title>Inscription</title>
	</head>
<!-- 
	margin -> marge avec les autres éléments
	padding -> marge intérieur de l'élément
-->
	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">

				<div class="col-md-6 mx-auto">
					<div class="card bg-light">
						<div class="card-header bg-dark text-white"> 
							Inscription
						</div>
						<div class="card-body">
							<form method="post" action="InscriptionController.php">
								<div class="form-group">
									<label for="login">Login</label>
									<input type="text" class="form-control" id="login" placeholder="Login" name="login" required>
								</div>

								<div class="form-group">
									<label for="password1">Mot de passe</label>
									<input type="password" class="form-control" id="password1" placeholder="Password" name="password1" required>
								</div>

								<div class="form-group">
									<label for="password2">Confirmation mot de passe</label>
									<input type="password" class="form-control" id="password2" placeholder="Confirmation password" name="password2" required>
								</div>

								<div class="form-group">
									<label for="tagRFID">Tag RFID</label>
									<input type="text" class="form-control" id="tagRFID" placeholder="tagRFID" name="tagRFID" required>
								</div>

								<?php 
								if(!empty($contenuAlerte)){
								?>
								<div class="alert alert-<?= $typeAlerte ?> alert-dismissible fade show mt-1" role="alert">
									<?= $contenuAlerte ?>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>			
								</div>
								<?php
								}
								?>

								<button type="submit" class="btn btn-dark">Valider</button>
							</form>
						</div>
					</div>
				</div>

			</div>

		</div>

		<?php require 'footer.php' ?>
		
	</body>

</html>