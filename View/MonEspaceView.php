<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://kit.fontawesome.com/14b05e12a0.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="style.css" />

		<title>Mon Espace</title>
	</head>
<!-- 
	margin -> marge avec les autres éléments
	padding -> marge intérieur de l'élément
-->
	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">
				<div class="col-sm-2 col-6 mx-auto border border-dark rounded text-center px-1 pt-1 text-nowrap">
					<h3><?= $solde ?> €</h3>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-6 mx-auto">
					<div class="list-group">
  						<a href="../Controller/AfficherOperationsController.php" class="list-group-item list-group-item-action">Opérations du compte</a>
  						<a href="../Controller/CrediterDebiterController.php" class="list-group-item list-group-item-action">Créditer ou débiter le compte</a>
 						<a href="../Controller/EffectuerVirementController.php" class="list-group-item list-group-item-action">Effectuer un virement</a>
					</div>
				</div>
			</div>

		</div>

		<?php require 'footer.php' ?>

	</body>

</html>