<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://kit.fontawesome.com/14b05e12a0.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="style.css" />

		<title>Vos Opérations</title>
	</head>

	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">
				<div class="col-sm-2 col-6 mx-auto border border-dark rounded text-center px-1 pt-1 text-nowrap">
					<h3><?= $solde ?> €</h3>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-sm-6 col-10 mx-auto">
					<?php
					if(!empty($listeOperations)){
					?>
					<ul class="list-group list-group-flush">

 						<?php
 						foreach ($listeOperations as $operation){
 						?>
 							<li class="list-group-item">
 								<h6><?= $operation->getDate() ?></h6>
 								 <?= $operation->getMontant() ?> € | <?= $operation->getCategorie() ?>
 							</li>
 						<?php
 						}
 						?>
					</ul>	
					<?php
					}
					else{
					?>
						<h4 class="text-center">Vous n'avez pas encore effectué d'opérations bancaires.</h4>
					<?php
					}
					?>			
				</div>
			</div>

		</div>

		<?php require 'footer.php' ?>
		
	</body>

</html>