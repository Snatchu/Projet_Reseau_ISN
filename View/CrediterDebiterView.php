<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://kit.fontawesome.com/14b05e12a0.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="style.css" />

		<title>Créditer / Débiter</title>
	</head>

	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">
				<div class="col-sm-2 col-6 mx-auto border border-dark rounded text-center px-1 pt-1 text-nowrap">
					<h3><?= $solde ?> €</h3>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-sm-6 col-11 mx-auto">
					<div class="card bg-light">
						<div class="card-header bg-dark text-white"> 
							Crediter / Débiter
						</div>
						<div class="card-body">
							<form method="post" action="AuthentificationController.php">
  								<div class="form-group row">
    								<label for="montant" class="col-sm-4 col-form-label">Montant</label>
    								<div class="col-sm-5">
      									<input type="number" name="montant" class="form-control" id="montant" placeholder="Montant" minlength="1" maxlength="6" required>
   									</div>
 								</div>
  								<div class="form-group row">
    								<label for="inputGroupSelect03" class="col-sm-4 col-form-label">Opération</label>
    								<div class="col-sm-5">
  										<div class="input-group">
  											<select class="custom-select" id="inputGroupSelect03" name="operation">
    											<option value="debit">Débit</option>
    											<option value="credit">Crédit</option>
  											</select>
										</div>    		
									</div>
  								</div>
  								<div class="form-group row">
    								<label for="intitule" class="col-sm-4 col-form-label">Intitulé</label>
    								<div class="col-sm-5">
      									<input type="text" name="intitule" class="form-control" id="intitule" placeholder="Intitulé" minlength="4" maxlength="20"required>
   									</div>
 								</div>
 								<?php 
								if(!empty($contenuAlerte)){
								?>
								<div class="alert alert-<?= $typeAlerte ?> alert-dismissible fade show mt-1" role="alert">
									<?= $contenuAlerte ?>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>			
								</div>
								<?php
								}
								?>
  								<button type="submit" class="btn btn-dark" name="url" value="CrediterDebiterController.php">Valider</button>
							</form>
						</div>
					</div>	
				</div>
			</div>

		</div>

		<?php require 'footer.php' ?>

	</body>

</html>