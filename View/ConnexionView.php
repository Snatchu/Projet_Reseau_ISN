<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="style.css" />

		<title>Connexion</title>
	</head>
<!-- 
	margin -> marge avec les autres éléments
	padding -> marge intérieur de l'élément
-->
	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="card bg-light">
						<div class="card-header bg-dark text-white"> 
							Connexion à votre espace
						</div>
						<div class="card-body">
							<form method="post" action="ConnexionController.php">
								<div class="form-group">
									<label for="loginInput">Login</label>
									<input type="text" class="form-control" id="loginInput" placeholder="Enter login" name="login" required>
								</div>

								<div class="form-group">
									<label for="exampleInputPassword1">Mot de passe</label>
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
								</div>

								<?php 
								if($affichageAlerteLoginMdp == true){
								?>
								<div class="alert alert-danger alert-dismissible fade show mt-1" role="alert">
									Login ou mot de passe incorrect.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>			
								</div>
								<?php
								}
								?>
								<button type="submit" class="btn btn-dark">Valider</button>
								<a href="InscriptionController.php" class="float-right text-dark pt-2">Pas encore inscrit ?</a>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php
			if($affichageAlerteDeconnexion == true){
			?>
			<div class="row">
				<div class="col-md-6 mx-auto">
					<div class="alert alert-secondary alert-dismissible fade show mt-1" role="alert">
						Vous avez été déconnecté.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>			
					</div>
				</div>
			</div>
			<?php
			}
			?>
			
		</div>

		<?php require 'footer.php' ?>
		
	</body>

</html>