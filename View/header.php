<header>
	<div class="bg-dark mb-5 p-2">
		<div class="container bg-black">
			<div class="row bg-black text-white">
				<div class="col-4 text-center p-0">
					<form method="GET" action="../Controller/MonEspaceController.php" class="p-0">
						<button class="btn btn-dark p-0 float-sm-none float-left " type="submit">SecurePay</button> 
					</form>
				</div>
				<div class="col-4 text-center p-0">
					<?= $page_title ?>
				</div>
				<?php
				if(isset($_SESSION['utilisateur'])){
				?>
				<div class="col-4 text-center p-0"> 
						<form method="POST" action="../Controller/ConnexionController.php" class="p-0">
							<input type="hidden" name="deconnexion">
							<button class="btn btn-dark p-0 float-sm-none float-right" type="submit">	
								<?= ucfirst(htmlspecialchars($_SESSION['utilisateur']->getLogin())) ?>
								<div class="fas fa-power-off"></div>
							</button> 
						</form>
				</div> 
				<?php 
				}
				?>
			</div>
		</div>
	</div>
</header>