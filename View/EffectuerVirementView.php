<!doctype html>
<html lang="fr">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://kit.fontawesome.com/14b05e12a0.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="style.css" />

		<title>Effectuer un virement</title>
	</head>

	<body>

		<?php include 'header.php' ?>

		<div class="container mb-5">

			<div class="row">
				<div class="col-sm-2 col-6 mx-auto border border-dark rounded text-center px-1 pt-1 text-nowrap">
					<h3><?= $solde ?> €</h3>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-sm-6  col-11 mx-auto">
					<div class="card bg-light">
						<div class="card-header bg-dark text-white"> 
							Effectuer un virement
						</div>
						<div class="card-body">
							<form method="post" action="AuthentificationController.php">
								<div class="row">
									<div class="col-md-3 text-center pt-1">
    									Verser
									</div>
    								<div class="col-md-4">
      									<input type="number" name="montant" class="form-control" placeholder="Montant" minlength="1" maxlength="6" required>
   									</div>
   									<div class="col-md-2  text-center pt-1">
      									€ à 
									</div>
    								<div class="col-md-3 ">
										<select class="custom-select" id="inputGroupSelect03" name="destinataire">
										<?php 
										foreach ($listeLogins as $login) {
										?>
											<option value="<?= $login ?>"><?= $login ?></option>
										<?php 
										}
										?>
										</select>
									</div>
  								</div>
  								<?php 
								if(!empty($contenuAlerte)){
								?>
								<div class="alert alert-<?= $typeAlerte ?> alert-dismissible fade show mt-1" role="alert">
									<?= $contenuAlerte ?>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>			
								</div>
								<?php
								}
								?>
  								<div class="row">
  									<div class="col pt-3 text-center">
  										<button type="submit" class="btn btn-dark" name="url" value="EffectuerVirementController.php">Valider</button>
  									</div>
  								</div>
							</form>
						</div>
					</div>	
				</div>
			</div>

		</div>

		<?php require 'footer.php' ?>
		
	</body>

</html>