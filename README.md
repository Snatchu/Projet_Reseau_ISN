NGUYEN Basile - LE VIENNESSE Ronan


Ce dépot héberge le projet réalisé dans le cadre du module "Service Réseaux" en 2ème année d'école à l'ESIGELEC (dominante Ingénieurie des Services Numériques).
A terme le but de ce projet est de réaliser un site internet hébergé sur un Raspberry Pi. Ce site permettra un utilisateur de s'incrire, se connecter
et d'afficher son solde bancaire. Au moyen d'un lecteur RFID (et d'une carte) il pourra s'autentifier pour procéder à des débits / crédits de son
compte ainsi qu'effectuer des virements vers d'autres utilisateurs.
