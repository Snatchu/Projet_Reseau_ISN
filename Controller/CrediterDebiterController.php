<?php 
require '../Model/DAO/DAO.php';
session_start();

$page_title = "Créditer ou débiter votre compte";
$solde = "";
$statutRequete = false;
$typeAlerte = "";
$contenuAlerte = "";

if(!isset($_SESSION['utilisateur'])){
	header('Location: ConnexionController.php');
  	exit();
}else{
	$myDAO = new DAO();

	if(isset($_SESSION['montant']) && isset($_SESSION['operation']) && isset($_SESSION['intitule']) && isset($_SESSION['authentification'])){

		if ($_SESSION['authentification'] == true){
			if($_SESSION['operation'] == "debit")
				$statutRequete = $myDAO->addOperation($_SESSION['utilisateur']->getLogin(), null, $_SESSION['intitule'], $_SESSION['montant']);
			else if($_SESSION['operation'] == "credit")
				$statutRequete = $myDAO->addOperation(null, $_SESSION['utilisateur']->getLogin(), $_SESSION['intitule'], $_SESSION['montant']);

			if($statutRequete){
				$typeAlerte = "secondary";
				$contenuAlerte = "Votre " . htmlspecialchars($_SESSION['operation']) . " de " . htmlspecialchars($_SESSION['montant']) . " € (" . htmlspecialchars($_SESSION['intitule']) . ")  de votre compte a été réalisé avec succès !" ;
			}else{
				$typeAlerte = "danger";
				$contenuAlerte = "Impossible d'effectuer l'opération !";
			}
		} else {
			$typeAlerte = "danger";
			$contenuAlerte = "Carte non reconnue !";
		}
		unset($_SESSION['montant']);
		unset($_SESSION['intitule']);
		unset($_SESSION['operation']);
		unset($_SESSION['authentification']);
	}

	$solde = htmlspecialchars($myDAO->soldeFromOperations($_SESSION['utilisateur']->getLogin()));
	require('../View/CrediterDebiterView.php');
}


/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 		
*/