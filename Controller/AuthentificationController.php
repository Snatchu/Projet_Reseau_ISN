<?php 
require '../Model/DAO/DAO.php';
session_start();

$page_title = "Authentification";
$montant = "";
$destinataire = "";
$authentification = 0;
$operation="";
$intitule="";
$url="";

if(!isset($_SESSION['utilisateur'])){
	header('Location: ConnexionController.php');
  	exit();
}else{

    $myDAO = new DAO();
    $tag=exec("sudo ../Model/DAO/reader");
    $url = $_POST['url'];

    if ($tag == $_SESSION['utilisateur']->getTag())
    {
        $authentification = 1;
        $montant = $_POST['montant'];

        if (strcmp($url, "EffectuerVirementController.php") == 0)
        {
            $destinataire =$_POST['destinataire'];
        } else if (strcmp($url, "CrediterDebiterController.php") == 0)
        {
            $operation = $_POST['operation'];
            $intitule = $_POST['intitule'];
        }
    }
    $_SESSION['authentification'] = $authentification;
    $_SESSION['montant'] = $montant;
    $_SESSION['destinataire'] = $destinataire;
    $_SESSION['operation'] = $operation;
    $_SESSION['intitule'] = $intitule;


    header('Location: '.$url);
    exit();
}

?>