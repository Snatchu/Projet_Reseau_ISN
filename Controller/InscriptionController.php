<?php 
require '../Model/DAO/DAO.php';
session_start();


$page_title = "Inscription";
$contenuAlerte = "";
$typeAlerte = "";

if(isset($_SESSION['utilisateur'])){
	header('Location: MonEspaceController.php');
  	exit();
}else{
	if(isset($_POST['login']) && isset($_POST['password1']) && isset($_POST['password2']) && isset($_POST['tagRFID'])){

		if($_POST['password1'] == $_POST['password2']){
			$myDAO = new DAO();

			if($myDAO->addUtilisateur($_POST['login'], $_POST['password1'], $_POST['tagRFID'])){
				$typeAlerte = "secondary";
				$contenuAlerte ="Inscription réussit !";
			}else{
				$typeAlerte = "danger";
				$contenuAlerte ="Enregistrement impossible en BDD !";
			}
		}
		else{
			$typeAlerte = "danger";
			$contenuAlerte ="Les deux mots de passe ne correspondent pas !";
		}

	}
	require('../View/InscriptionView.php');
}

/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 
*/