<?php 
require '../Model/DAO/DAO.php';
session_start();

$page_title = "Effectuer un virement";
$solde = "";
$typeAlerte = "";
$contenuAlerte = "";

if(!isset($_SESSION['utilisateur'])){
	header('Location: ConnexionController.php');
  	exit();
}else{
	$myDAO = new DAO();
	if(isset($_SESSION['montant']) && isset($_SESSION['destinataire']) && isset($_SESSION['authentification'])){

		if ($_SESSION['authentification'] == true){
			if($myDAO->addOperation($_SESSION['utilisateur']->getLogin(), $_SESSION['destinataire'], "virement", $_SESSION['montant'])){
				$typeAlerte = "secondary";
				$contenuAlerte = "Votre virement de " . htmlspecialchars($_SESSION['montant']) . " € vers " . htmlspecialchars($_SESSION['destinataire']) . " a bien été effectué !";
			}else{
				$typeAlerte = "danger";
				$contenuAlerte = "Impossible d'effectuer le virement !";
			}
		} else {
			$typeAlerte = "danger";
			$contenuAlerte = "Carte non reconnue !";
		}
		unset($_SESSION['montant']);
		unset($_SESSION['destinataire']);
		unset($_SESSION['authentification']);
	}


	$listeLogins = $myDAO->getListLogin();
	unset($listeLogins[array_search($_SESSION['utilisateur']->getLogin(), $listeLogins)]);



	$solde = htmlspecialchars($myDAO->soldeFromOperations($_SESSION['utilisateur']->getLogin()));
	require('../View/EffectuerVirementView.php');
}

/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 		
*/