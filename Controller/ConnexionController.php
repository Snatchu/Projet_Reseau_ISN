<?php 
require '../Model/DAO/DAO.php';
session_start();

/*
Ces instruction de contrôle vérifie si l'utilisateur est déja connecté ou non, et le renvoit vers sonEspace s'il est connecté (sauf si on est dans le cas d'une déconnexion).
Dans le cas contraire le controller vérifie si on est dans le cas où l'on reçois des données ou non et traite ses données dans le premier cas et affiche la page (avec un require()) dans le cas contraire.
*/

$page_title = "Connexion";
$affichageAlerteLoginMdp = false;
$affichageAlerteDeconnexion = false;

if(isset($_SESSION['utilisateur'])){
	if(isset($_POST['deconnexion'])){
		session_destroy();
		/*session_destroy() détruit la session, mais ne vide pas les valeurs de l'array $_SESSION. 
		On s'en charge donc dans la ligne suivante, afin d'éviter des bugs avec les isset($_SESSION['login']) par exemple
		*/
		$_SESSION = array();
		$affichageAlerteDeconnexion = true;
		require('../View/ConnexionView.php');
	}
	else{
		header('Location: MonEspaceController.php');
  		exit();
	}

}else{
	if(isset($_POST['login']) && isset($_POST['password'])){
		$myDAO = new DAO();
		if($myDAO->validationCredentials($_POST['login'], $_POST['password'])){
			$_SESSION['utilisateur'] = $myDAO->getUtilisateur($_POST['login']); 
			header('Location: MonEspaceController.php');
			exit();
		}
		else{
			$affichageAlerteLoginMdp = true;
			require('../View/ConnexionView.php');
		}
	}
	else{
		require('../View/ConnexionView.php');
	}
}

/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 
*/