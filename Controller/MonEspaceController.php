<?php 
require '../Model/DAO/DAO.php';
session_start();

$page_title = "Mon Espace";
$solde = "";

if(!isset($_SESSION['utilisateur'])){
	header('Location: ConnexionController.php');
  	exit();
}else{

	$myDAO = new DAO();
	$solde = htmlspecialchars($myDAO->soldeFromOperations($_SESSION['utilisateur']->getLogin()));
	require('../View/MonEspaceView.php');
}

/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 
*/