<?php 
require '../Model/DAO/DAO.php';
session_start();

$page_title = "Vos opérations";
$solde = "";
$listeOperations;

if(!isset($_SESSION['utilisateur'])){
	header('Location: ConnexionController.php');
  	exit();
}else{
	$myDAO = new DAO();
	$solde = htmlspecialchars($myDAO->soldeFromOperations($_SESSION['utilisateur']->getLogin()));

	$listeOperations =  $myDAO->getOperationsUser($_SESSION['utilisateur']->getLogin());

	foreach ($listeOperations as $operation) {

		$operation->login_emetteur = htmlspecialchars($operation->login_emetteur);
    	$operation->login_destinataire = htmlspecialchars($operation->login_destinataire);
    	$operation->categorie = htmlspecialchars($operation->categorie);
    	$operation->montant = htmlspecialchars($operation->montant);
    	$operation->date = htmlspecialchars($operation->date);

		if($operation->getLoginEmetteur() == $_SESSION['utilisateur']->getLogin()){
			$operation->montant = "- " . $operation->montant;
			if($operation->getCategorie() == "virement")
				$operation->categorie = "virement vers " . $operation->getLoginDestinataire();
		}
		else if($operation->getLoginDestinataire() == $_SESSION['utilisateur']->getLogin()){
			$operation->montant = "+ " . $operation->montant;
			if($operation->getCategorie() == "virement")
				$operation->categorie = "virement de " . $operation->getLoginEmetteur();
		}	

	}
	$listeOperations = array_reverse($listeOperations);

	require('../View/AfficherOperationsView.php');
}

/* Balise <?php non fermée -> se réferer à https://bpesquet.developpez.com/tutoriels/php/evoluer-architecture-mvc/ 
II-A-1. Isolation de l'affichage 		
*/