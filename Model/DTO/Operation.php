<?php

class Operation
{
    public $login_emetteur;
    public $login_destinataire;
    public $categorie;
    public $montant;
    public $date;

    public function __construct($_login_emetteur, $_login_destinataire, $_categorie, $_montant, $_date)
    {

        if ($_login_emetteur == null){
            $this ->login_emetteur = "Non renseigné";
        } else {
            $this ->login_emetteur = $_login_emetteur;
        }
        if ($_login_destinataire == null){
            $this ->login_destinataire = "Non renseigné";
        } else {
            $this ->login_destinataire = $_login_destinataire;
        }
        $this ->categorie = $_categorie;
        $this ->montant = $_montant;
        $this ->date = $_date;
    }

    public function getLoginEmetteur()
    {
        return $this ->login_emetteur;
    }

    public function getLoginDestinataire()
    {
        return $this ->login_destinataire;
    }

    public function getCategorie()
    {
        return $this ->categorie;
    }

    public function getMontant()
    {
        return $this ->montant;
    }

    public function getDate()
    {
        return $this ->date;
    }

    public function __toString()
    {
       return "De ".$this ->login_emetteur." vers ".$this ->login_destinataire." de type : ".
       $this ->categorie." de ".$this ->montant." le ".$this ->date."<br/>";
    }
}
?>