<?php

class Utilisateur
{
    private $login;
    private $password;
    private $tag;
    private $solde;

    public function __construct($_login, $_password, $_tag, $_solde)
    {
        $this->login = $_login;
        $this->password = $_password;
        $this->tag = $_tag;
        $this->solde = $_solde;
    }

    public function getSolde()
    {
        return $this->solde;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPassword()
    {
        return $this->password;
    }
}

?>