<?php
require_once('../Model/DTO/Utilisateur.php');
require_once('../Model/DTO/Operation.php');
class DAO
{
  public $pdo;
  public $url = "mysql:host=localhost;dbname=banque;charset=utf8";
  public $login = "admin_banque";
  public $password = "6pemwC@+p-vV3r.";

  public function __construct()
  {
    global $pdo;
    $pdo = new PDO($this->url, $this->login, $this->password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  }


  public function getIDUtilisateur($login)
  {
    global $pdo;
    try
    {
      $data = $pdo ->prepare("SELECT id FROM utilisateurs WHERE login = ?");
      $data->bindParam(1,$login);

      $data->execute();
      $id = $data -> fetch();

      return $id[0];
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }


  public function getLogUtilisateur($id)
  {
    global $pdo;
    try
    {
      $data = $pdo ->prepare("SELECT login FROM utilisateurs WHERE id = ?");
      $data->bindParam(1,$id);

      $data->execute();
      $log = $data -> fetch();

      return $log[0];
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }

  /**
   * permet de retourner l'heure dans le format 17h30
   */
  public function getActualHour()
  {
    date_default_timezone_set("Europe/Paris");
    $hour = date("H").'h'.date("i");
    return $hour;
  }


  /**
   * Obtention du login à partir du tag rfid
   * @param tag de la carte
   * @return login du propriétaire de la carte dasn la BDD
   */
  public function getLoginUtilisateur($tag)
  {
    global $pdo;
    try
    {
      $data = $pdo ->prepare("SELECT login FROM utilisateurs WHERE tag_rfid = ?");
      $data->bindParam(1,$tag);

      $data->execute();
      $log = $data -> fetch();

      return $log[0];
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }


  /**
   * Récupère le mdp dans la bdd lié au login input
   * @param login nom de l'user rentré
   * @return le mdp associé dans la bdd
   */
  public function getMdpUtilisateur($login)
  {
    global $pdo;
    try
    {
      $data = $pdo ->prepare("SELECT mot_de_passe FROM utilisateurs WHERE login = ?");
      $data->bindParam(1,$login);

      $data->execute();
      $mdp = $data -> fetch();

      return $mdp[0];
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }


  /**
   * Fonction de comparaison entre le mdp en input et le mdp dans la bdd lié au login
   * @param login de l'user
   * @param mdp rentré par l'user
   * @return true si les mdp correspondent, false sinon
   */
  public function validationCredentials($login,$mdp)
  {
    $hashmdp = password_hash($mdp, PASSWORD_DEFAULT);
    return password_verify($mdp, DAO::getMdpUtilisateur($login));
  }


  /**
   * Crée un objet utilisateur avec toutes les infos présentes dans la bdd à partir de son login
   * @param login 
   * @return utilisateur 
   */
  public function getUtilisateur($login)
  {
    global $pdo;
    try
    {
      $data = $pdo ->prepare("SELECT * FROM utilisateurs WHERE login = ?");
      $data->bindParam(1,$login);

      $data->execute();
      $infos = $data -> fetch();

      $log = $infos['login'];
      $password = $infos['mot_de_passe'];
      $tag =  $infos['tag_rfid'];
      $solde = DAO::soldeFromOperations($login);
      return new Utilisateur($log, $password, $tag, $solde);;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }


  /**
   * Liste les login des utilisateurs dans la bdd
   * @return array de login
   */
  public function getListLogin()
  {
    global $pdo;
    try
    {
      $data = $pdo ->query('SELECT login FROM utilisateurs');   // pas de paramètres input, on passe par un query

      $users = array();
      foreach ($data as $row)
      {
        array_push($users, $row['login']);                      // on met les éléments dans un tableau un par un
      }
      return $users;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }


  /**
   * Permet de vérifier si l'utilisateur est déjà présent dans la bdd
   * @return true si l'insertion a eu lieu
   *         false si le login est déjà présent dans la bdd
   */
  public function checkAddUtilisateur($login)
  {
    if(in_array($login,DAO::getListLogin()))
    {
      return false;
    }else{
      //DAO::addUtilisateur($login, $mdp, $tag);
      return true;
    }
  }


  /**
   * Permet d'ajouter un utilisateur dans la bdd
   * @param login du nouvel utilisateur
   * @param mdp de l'utilisateur
   * @param tag de la carte lié à  cet utilisateur
   * @return binaire true si l'utilisateur a été ajouté correctement
   */
  public function addUtilisateur($login, $mdp, $tag)
  {
    global $pdo;
    $hashmdp = password_hash($mdp, PASSWORD_DEFAULT);
    try
    {
      $data = $pdo ->prepare("INSERT INTO utilisateurs (login, mot_de_passe, tag_rfid) VALUES (?, ?, ?)");
      $data->bindParam(1,$login);
      $data->bindParam(2,$hashmdp);
      $data->bindParam(3,$tag);

      $data->execute();

      return true;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
      return false;
    }
    finally
    {
      $data = null;
    }
  }


  /**
   * Permet d'insérer une opération dans la bdd
   * @param login de l'emetteur de la transaction
   * @param login du destinataire de la transaction
   * @param categorie de la transaction, chaine de caractère : loyer, salaire...
   * @param montant de la transaction
   * @return true si l'insertion a réussi
   */
  public function addOperation($login_emetteur, $login_destinataire, $categorie, $montant)
  {
    global $pdo;
    $log_em = DAO::getIDUtilisateur($login_emetteur);
    $log_dest =DAO::getIDUtilisateur($login_destinataire);
    date_default_timezone_set("Europe/Paris");
    $date = date("d-m-Y")." ".DAO::getActualHour();

    if (DAO::checkAccountOperation($login_emetteur, $montant))
    {
      if (($log_em == null) && ($log_dest == null)){}
      else {
        try 
        {
          $data = $pdo ->prepare("INSERT INTO operations 
          (id_emetteur, id_destinataire, categorie, montant, date) VALUES (?, ?, ?, ?, ?)");
          $data->bindParam(1,$log_em);
          $data->bindParam(2,$log_dest);
          $data->bindParam(3,$categorie);
          $data->bindParam(4,$montant);
          $data->bindParam(5,$date);

          $data->execute();
          return true;
        }
        catch(PDOException $e) 
        {
          echo "Error: " . $e->getMessage();
          return false;
        }
        finally
        {
          $data = null;
        }
      }
    }  
  }

  /**
   * permet de savoir si un utilisateur a le solde nécessaire pour faire une transaction
   * @param login de l'user
   * @param montant de la transaction
   * @return true si l'utilisateur peut faire la transation false sinon
   */
  public function checkAccountOperation($login_emetteur, $montant)
  {
    if ($login_emetteur == null)
    {
      return true;
    }else{
      if (DAO::soldeFromOperations($login_emetteur)<$montant)
      {
        return false;
      } else {
        return true;
      }
    }  
  }


  /**
   * Liste les opérations d'un utilisateur où il était destinataire
   * @param login de l'user
   * @return array objets Opérations 
   */
  public function getOperationsCredit($login)
  {
    global $pdo;
    $id = DAO::getIDUtilisateur($login);
    try
    {
      $operations = array();
      $data = $pdo ->prepare("SELECT * FROM operations WHERE id_destinataire = ?");
      $data->bindParam(1,$id);

      $data->execute();

      foreach ($data as $row)
      {
        $log_emetteur = DAO::getLogUtilisateur($row['id_emetteur']);
        $ope = new Operation($log_emetteur, $login, $row['categorie'], $row['montant'], $row['date']);

        array_push($operations, $ope);
      }
      return $operations;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }

   /**
   * Liste les opérations d'un utilisateur où il était emetteur
   * @param login de l'user
   * @return array objets Opérations 
   */
  public function getOperationsDebit($login)
  {
    global $pdo;
    $id = DAO::getIDUtilisateur($login);
    try
    {
      $operations = array();
      $data = $pdo ->prepare("SELECT * FROM operations WHERE id_emetteur = ?");
      $data->bindParam(1,$id);

      $data->execute();

      foreach ($data as $row)
      {
        $log_destinataire = DAO::getLogUtilisateur($row['id_destinataire']);
        $ope = new Operation($login, $log_destinataire, $row['categorie'], $row['montant'], $row['date']);

        array_push($operations, $ope);
      }
      return $operations;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }

  /**
   * permet de calculer le solde d'un user par addtion et soustraction de toutes ses opérations
   * @param login de l'utilisateur
   * @return solde de l'utilisateur 
   */
  public function soldeFromOperations($login)
  {
    $credit = DAO::getOperationsCredit($login);
    $debit = DAO::getOperationsDebit($login);
    $solde = 0;

    foreach ($credit as $credit){
      $solde = $solde + $credit->getMontant();
    }

    foreach ($debit as $debit){
      $solde = $solde - $debit->getMontant();
    }

    return $solde;
  }


  /**
   * Liste toutes les opérations d'un utilisateur
   * @param login de l'user
   * @return array objets Opérations 
   */
  public function getOperationsUser($login)
  {
    global $pdo;
    $id = DAO::getIDUtilisateur($login);
    try
    {
      $operations = array();
      $data = $pdo ->prepare("SELECT * FROM operations WHERE id_emetteur = ? OR id_destinataire = ?");
      $data->bindParam(1,$id);
      $data->bindParam(2,$id);

      $data->execute();

      foreach ($data as $row)
      {
        $log_destinataire = DAO::getLogUtilisateur($row['id_destinataire']);
        $log_emetteur = DAO::getLogUtilisateur($row['id_emetteur']);
        $ope = new Operation($log_emetteur, $log_destinataire, $row['categorie'], $row['montant'], $row['date']);

        array_push($operations, $ope);
      }
      return $operations;
    }
    catch(PDOException $e) 
    {
      echo "Error: " . $e->getMessage();
    }
    finally
    {
      $data = null;
    }
  }

  
}
?>